package doublea.smsvcard;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
//import android.util.Log;
import android.net.Uri;

import android.telephony.SmsMessage;
import android.annotation.SuppressLint;

import android.app.NotificationManager;
import android.app.Notification;
import android.support.v4.app.NotificationCompat.Builder;

import java.util.UUID;
import java.io.FileOutputStream;
import java.io.File;

import android.app.PendingIntent;

public class SmsReceiver extends BroadcastReceiver {

	public static final String TAG = "SMS Receiver";
	
	public static final String SMS_EXTRA_NAME = "pdus";
	public static final String SMS_URI = "content://sms";
	
	public static final String ADDRESS = "address";
    public static final String PERSON = "person";
    public static final String DATE = "date";
    public static final String READ = "read";
    public static final String STATUS = "status";
    public static final String TYPE = "type";
    public static final String BODY = "body";
    public static final String SEEN = "seen";
    
    public static final int MESSAGE_TYPE_INBOX = 1;
    public static final int MESSAGE_TYPE_SENT = 2;
    
    public static final int MESSAGE_IS_NOT_READ = 0;
    public static final int MESSAGE_IS_READ = 1;
    
    public static final int MESSAGE_IS_NOT_SEEN = 0;
    public static final int MESSAGE_IS_SEEN = 1;
	
	@SuppressLint({ "WorldReadableFiles", "WorldReadableFiles" })
	public void onReceive( Context context, Intent intent ) 
    {
		if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
			Bundle extras = intent.getExtras();
			if ( extras != null ) {
                String body = ""; 
                String address = "";
				Object[] smsExtra = (Object[]) extras.get("pdus");
				
				final SmsMessage[] msgs = new SmsMessage[smsExtra.length];
				
				for (int n = 0; n < smsExtra.length; n++) {
					msgs[n] = SmsMessage.createFromPdu((byte[])smsExtra[n]);
					body += msgs[n].getMessageBody().toString();
					address = msgs[n].getOriginatingAddress();
				}
				
	            if (body.startsWith("//SCKL23F4") || body.startsWith("BEGIN:VCARD")) {
                	body = trimvCard(body);
                	
                	// Store temporary vcf
                	UUID msgId = UUID.randomUUID();
                	String FILENAME = msgId.toString();
                	int idHash = msgId.hashCode();
                	
                	FileOutputStream fos = null;
                	try {
                		fos = context.openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
                		fos.write(body.getBytes());
            			fos.flush();
            			fos.close();
                	}
                	catch (Exception e) {
                		// TODO Auto-generated catch block
                		e.printStackTrace();
                	}
                	File internalFile = context.getFileStreamPath(FILENAME);
                	Uri fUri = Uri.fromFile(internalFile);
                	
                	/*
                	Intent i = new Intent();
            		i.setAction(android.content.Intent.ACTION_VIEW);
            		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            		i.setDataAndType(fUri, "text/x-vcard");
            		context.startActivity(i);
                	this.abortBroadcast(); // handled SMS.. can stop broadcasting
                	*/
	                
                	// Notify user 
                	// http://developer.android.com/guide/topics/ui/notifiers/notifications.html
                	// http://developer.android.com/reference/android/app/Notification.Builder.html
                	NotificationManager nMngr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                	
                	int icon = android.R.drawable.stat_notify_chat;
                	CharSequence tickerText = "vCard from " + address;
                	long when = System.currentTimeMillis();
                	// Create intent to launch whatever vCard import utility is installed
                	Intent nIntent = new Intent()
                		.setAction(android.content.Intent.ACTION_VIEW)
                		.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                		.setDataAndType(fUri, "text/x-vcard");
                	// Make into pending intent
                	PendingIntent cIntent = PendingIntent.getActivity(context, 0, nIntent, 0);
                	// create the notification
                	Notification notif = new Builder(context)
                		.setContentTitle("Received vCard")
                		.setContentText(tickerText)
                		.setSmallIcon(icon)
                		.setWhen(when)
                		.setContentIntent(cIntent)
                		.build();
                	notif.defaults |= Notification.DEFAULT_SOUND;
                	notif.defaults |= Notification.DEFAULT_VIBRATE;
                	notif.defaults |= Notification.DEFAULT_LIGHTS;

                	nMngr.notify(idHash, notif);
                	
                	// TODO: Prevent garbling up the users SMS inbox
                	// sms-handling apps from getting messages.
                	/* 
                	 * WARNING!!! 
    		         *	 If you uncomment the next line then received SMS will not be put to incoming.
    		         *   Be careful!
                	 */
    		        // this.abortBroadcast(); 
                	
                	// Notify user
                	/*
                	StringBuilder buf = new StringBuilder();
                	
                    buf.append("Received vCard from ");
                    buf.append(address);
                                        
                    Log.i(TAG, buf.toString());
                    Notification nb = new Notification();
                    nb.defaults = Notification.DEFAULT_ALL;
                    nb.tickerText = buf.toString();
                    
                    NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                    nm.notify(null, idHash, nb);

	                // Create SMS row
	                ContentValues values = new ContentValues();
	                 
                    values.put( ADDRESS, sms.getOriginatingAddress() );
                    values.put( DATE, sms.getTimestampMillis() );
                    values.put( READ, MESSAGE_IS_NOT_READ );
                    values.put( STATUS, sms.getStatus() );
                    values.put( TYPE, MESSAGE_TYPE_INBOX );
                    values.put( SEEN, MESSAGE_IS_NOT_SEEN );
                    values.put( BODY, body );   
                    // Push row into the SMS table
                    contentResolver.insert( Uri.parse( SMS_URI ), values );
                    */
            	}
			}
		}
		if (intent.getAction().equals("android.intent.action.DATA_SMS_RECEIVED")) {		
			Bundle extras = intent.getExtras();
			
			String body = ""; 
            String address = "";
            if (extras != null) {
				Object[] smsExtra = (Object[]) extras.get("pdus");
				SmsMessage[] msgs = new SmsMessage[smsExtra.length];
				
				for (int n = 0; n < smsExtra.length; n++) {
					msgs[n] = SmsMessage.createFromPdu((byte[])smsExtra[n]);
				}
				for (SmsMessage msg : msgs) {
					if (!msg.isStatusReportMessage()) {
						// body += msg.getDisplayMessageBody();
						byte[] messageByteArray = msg.getPdu();
						// skipping PDU header, keeping only message body
	                    int x = 1 + messageByteArray[0] + 19 + 7;
	                    body += new String(messageByteArray, x, messageByteArray.length-x); // vCard info
	                    address = msg.getOriginatingAddress();
					}
				}
            }
            if (body.startsWith("//SCKL23F4") || body.startsWith("BEGIN:VCARD")) {
            	body = trimvCard(body);
            	
            	// Store temporary vcf
            	UUID msgId = UUID.randomUUID();
            	String FILENAME = msgId.toString();
            	int idHash = msgId.hashCode();
            	
            	FileOutputStream fos = null;
            	try {
            		fos = context.openFileOutput(FILENAME, Context.MODE_WORLD_READABLE);
            		fos.write(body.getBytes());
        			fos.flush();
        			fos.close();
            	}
            	catch (Exception e) {
            		// TODO Auto-generated catch block
            		e.printStackTrace();
            	}
            	File internalFile = context.getFileStreamPath(FILENAME);
            	Uri fUri = Uri.fromFile(internalFile);
            	
            	// Notify user 
            	// http://developer.android.com/guide/topics/ui/notifiers/notifications.html
            	// http://developer.android.com/reference/android/app/Notification.Builder.html
            	NotificationManager nMngr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            	
            	int icon = android.R.drawable.stat_notify_chat;
            	CharSequence tickerText = "vCard from " + address;
            	long when = System.currentTimeMillis();
            	// Create intent to launch whatever vCard import utility is installed
            	Intent nIntent = new Intent()
            		.setAction(android.content.Intent.ACTION_VIEW)
            		.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            		.setDataAndType(fUri, "text/x-vcard");
            	// Make into pending intent
            	PendingIntent cIntent = PendingIntent.getActivity(context, 0, nIntent, 0);
            	// create the notification
            	Notification notif = new Builder(context)
            		.setContentTitle("Received vCard")
            		.setContentText(tickerText)
            		.setSmallIcon(icon)
            		.setWhen(when)
            		.setContentIntent(cIntent)
            		.build();
            	notif.defaults |= Notification.DEFAULT_SOUND;
            	notif.defaults |= Notification.DEFAULT_VIBRATE;
            	notif.defaults |= Notification.DEFAULT_LIGHTS;

            	nMngr.notify(idHash, notif);

            	// WARNING!!! 
		        // If you uncomment the next line then received SMS will not be put to incoming.
		        // Be careful!
		        // this.abortBroadcast(); 
            }
        }
    }
	private String trimvCard(String vc) {
		if (vc.startsWith("//SCKL23F4")) {
			vc = vc.substring(10).trim();
		}
		return vc;
	}
	
}
