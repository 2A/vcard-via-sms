package doublea.smsvcard;

import android.os.Bundle;
import android.app.Activity;
// import android.util.Log;

import android.content.Intent;
import android.net.Uri;
import android.database.Cursor;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;

import android.content.ContentResolver;
import android.provider.ContactsContract;

import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.CommonDataKinds;  

import android.telephony.SmsManager;

import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.app.AlertDialog;
import android.widget.Toast;

public class SendSMSActivity extends Activity {
	
	public static final String TAG = "SMS Sender";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        final ContentResolver resolver = getContentResolver();
        
        /* CONTACT DATA TO BE SENT */
        String cDName = null; // Contacts display name
        ArrayList< CharSequence > numbers = new ArrayList<CharSequence>();
        final ArrayList< String > noStrs = new ArrayList<String>();
        
        // Receive intent from contact's share picker
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        final Uri uri = (Uri)extras.get(Intent.EXTRA_STREAM); // get vcard uri
        
        // Extract lookup key from vcard uri
        String lookupKey = uri.toString().substring(ContactsContract.Contacts.CONTENT_VCARD_URI.toString().length()+1);
        // Try getting contact uri with lookup key 
        Uri contactUri = null;
        boolean isContact = false;
        try {
        	contactUri = ContactsContract.Contacts.lookupContact(getContentResolver(), Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_LOOKUP_URI, lookupKey));
        	isContact = true;
        }
        catch(IllegalArgumentException e) {
        	// Invalid lookup key (possibly not a contact, but a file)
        	// Attempt to find names numbers from the vCard file.
        	try {
            	AssetFileDescriptor afd = resolver.openAssetFileDescriptor(uri, "r");
            	FileInputStream fis = afd.createInputStream();
            	
            	int afdl = (int)afd.getDeclaredLength();
            	
            	if (afdl == AssetFileDescriptor.UNKNOWN_LENGTH) afdl = (int) afd.getLength(); 
            	byte[] buf = new byte[afdl];
                fis.read(buf);
                String vCard = new String(buf);
                
                cDName = vCardGetName(vCard);
                String[] ns = vCardGetNumbers(vCard);
                for (int n = 0; n < ns.length; n++) {
                	noStrs.add(ns[n]);
              		numbers.add(Integer.toString(n) + ": " + ns[n]);
                }
            }
            catch (FileNotFoundException ee) {
            	Toast.makeText(getApplicationContext(), "Error: File not found.", Toast.LENGTH_LONG).show();
            	finish();
            }
            catch (IOException ee) {
            	Toast.makeText(getApplicationContext(), "Error: Cannot read file.", Toast.LENGTH_LONG).show();
            	finish();
            }
        }
        if (isContact) {
	        // Get contact's info
	        String cid = null;
	        Cursor cur = null;
	        cur = resolver.query(contactUri, null, null, null, null);
	        if (cur.getCount() > 0) {
	        	cur.moveToFirst();
	        	// Display name 
	        	cDName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)); // "display_name"));
	        	// cDName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)); // "display_name_primary"));
	        	while (!cur.isAfterLast()) {
	        		// get phone numbers
	            	int hasPhone = cur.getInt(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)); //"has_phone_number"));
	        		cid = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));  // "_id"));
	            	if (hasPhone > 0) {
	                  	Cursor pCur = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", new String[]{cid}, null);
	                  	while (pCur.moveToNext()) {
	                  		String no = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	                  		int type = pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
	                  		String label = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.LABEL));
	                  		String typel = ContactsContract.CommonDataKinds.Phone.getTypeLabel(getApplicationContext().getResources(), type, label).toString();
	                  		if (pCur.getInt(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.IS_PRIMARY)) > 0) {
	                  			typel += "(*)";
	                  		}
	                  		noStrs.add(no);
	                  		numbers.add(typel + ": " + no);
	                  	}
	                  	pCur.close();
	            	}
	            	cur.moveToNext();
	            }
	        }
	        else {
	        	Toast.makeText(getApplicationContext(), "Error: Contact not found.", Toast.LENGTH_LONG).show();
	        	finish();
	        }
	        
	        cur.close();
    	}

        // Have user select number if necessary.
        if (numbers.size() > 1) {
	        final CharSequence[] items = new CharSequence[numbers.size()];
	        for (int n=0; n < numbers.size(); n++) {
	        	items[n] = numbers.get(n);		
	        }
	        AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setTitle("Select number\r\n" + cDName);
	        final String cdn = cDName;
	        builder.setItems(items, new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int item) {
	            	// Parse vCard
	                vCardString = "BEGIN:VCARD\r\nVERSION:2.1\r\n";
	                vCardString += "FN:" + cdn + "\r\n";
	                vCardString += "TEL:" + parseNumber(noStrs.get(item)) + "\r\n";
	                vCardString += "END:VCARD\r\n";
	                
	                // Pick recipient
	                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
	                contactPickerIntent.setType(CommonDataKinds.Phone.CONTENT_TYPE);
	                startActivityForResult(contactPickerIntent, 1001);
	            }
	        });
	        builder.show();
        }
        else {
        	vCardString = "BEGIN:VCARD\r\nVERSION:2.1\r\n";
            vCardString += "FN:" + cDName + "\r\n";
            if (noStrs.size() > 0) {
            	vCardString += "TEL:" + parseNumber(noStrs.get(0)) + "\r\n";
            }
            vCardString += "END:VCARD\r\n";
            // Pick recipient
            Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);
            contactPickerIntent.setType(CommonDataKinds.Phone.CONTENT_TYPE);
            startActivityForResult(contactPickerIntent, 1001);
        } 
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            Uri uri = data.getData();

            if (uri != null) {
                Cursor c = null;
                try {
                    c = getContentResolver().query(uri, new String[]{
                                CommonDataKinds.Phone.NUMBER,
                                CommonDataKinds.Phone.DISPLAY_NAME },
                            null, null, null);
                    if (c != null && c.moveToFirst()) {
                        number = c.getString(0);
                        // String rn = c.getString(1);
                        // SEND SMS
                        SendSms();
                    }
                } 
                finally {
                    if (c != null) {
                        c.close();
                    }
                }
            }
        }
        finish();
    }
    
    private void SendSms() {
    	SmsManager sms = SmsManager.getDefault();
    	short SMS_PORT = 9204; // default port for vCard 
    	if (vCardString == null || vCardString == "") {
    		Toast.makeText(getApplicationContext(), "Error: Empty vCard message.\n Message not sent", Toast.LENGTH_LONG).show();
    		return;
    	}
    	else if (number == null || number == "") {
    		Toast.makeText(getApplicationContext(), "Error: No recipient number.\n Message not sent", Toast.LENGTH_LONG).show();
    		return;
    	}
    	else {
    		sms.sendDataMessage(number, null, SMS_PORT, vCardString.getBytes(), null, null);
    		Toast.makeText(getApplicationContext(), "vCard sent", Toast.LENGTH_LONG).show();
    	}
    }
    
    // Strip  unwanted characters from number
    // android formatting may be incompatible
    private String parseNumber(String in) {
    	String out =  in.replaceAll("[^\\d+]", "");
    	return out;
    }
    
    private String vCardGetName(String in) {
    	String[] in_ = in.split("\\r?\\n");
    	String out = "";
    	boolean vc = false;
    	for (int n=0; n < in_.length; n++) {
	    	if (vc) {
	    		if (in_[n].startsWith("FN:")) {
	    			out = in_[n].substring(3).trim();
	    			break;
	    		}
	    		else if (in_[n].startsWith("N:")) {
	    			out = in_[n].substring(2).trim();
	    		}
	    	}
    		if (in_[n].startsWith("BEGIN:VCARD")) vc = true;
    		if (in_[n].startsWith("VCARD:END")) break;
    	}
    	return out;
    }
    
    private String[] vCardGetNumbers(String in) {
    	String[] in_ = in.split("\\r?\\n");
    	String out = "";
    	boolean vc = false;
    	for (int n=0; n < in_.length; n++) {
    		if (vc) {
	    		if (in_[n].startsWith("TEL")) {
	    			String[] tmpStr = in_[n].split(":", 2);
	    			if (tmpStr.length > 1) {
	    				if (out != "") out += "�";
	    				out += tmpStr[1]; 
	    			}
	    		}
    		}
    		if (in_[n].startsWith("BEGIN:VCARD")) vc = true;
    		if (in_[n].startsWith("VCARD:END")) break;
    	}
    	return out.split("�");
    }
    
    private String number;
    private String vCardString;
    
    
}

