Send and receive vCards via SMS messages.

Application allows vCard to be shared as an SMS through contact's or vCard-files' share menu. VCard is sent as a single binary data message. Due to size restriction and compatibility with older phones, only one name and one number is included in the message.

When incoming SMS contains a vCard, application catches it. User is then
notified and can launch whichever vCard utility is installed. Application
handles both data and text SMS.

known issues

* Not being actively developed.
* No support for vCard-secure. (I have no equipment to test this functionality.)
* Should ask for confirmation before sending SMS, because it's easy to misclick with touchscreens.
* Won't work for sharing multiple contacts at the same time. (Option is there, but functionality is not.)
* Got some crash reports, which I'm unable to replicate, from Sony Ericsson Xperia users. :(